
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ShopsService } from '../services/shops.service';


@Component({
  selector: 'app-shops',
  templateUrl: './shops.component.html',
  styleUrls: ['./shops.component.css']
})
export class ShopsComponent   {

    shops : any[] = [];

    constructor(private route :Router, private shopsService: ShopsService) {

    }

    ngOnInit(): void {
      this.getshops();
    }

    getshops() {
      this.shopsService.getShops().subscribe( data => {
        this.shops = data;
      });
    }
}
