import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BrandsService } from '../services/brands.service';


@Component({
  selector: 'app-brands',
  templateUrl: './brands.component.html',
  styleUrls: ['./brands.component.css']
})
export class BrandsComponent   {

    brands : any = [];

    constructor(private route :Router, private  brandsService:BrandsService) {

    }

    ngOnInit(): void {
      this.getbrands();
    }

    getbrands(){
      this.brandsService.getBrands().subscribe( data => {
        this.brands = data;
      });
    }
}

