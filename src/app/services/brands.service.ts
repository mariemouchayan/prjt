
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BrandsService {
  
  httpOptions = {
    headers: new HttpHeaders({
      'content-type': 'application/json',
      'accept': 'application/json'
    })
  };

  constructor(private http: HttpClient) { 

  }

  getBrands(): Observable<any[]> {
    return this.http
      .get<any[]>(environment.apiUrl + "/brands", this.httpOptions);
  }

}


