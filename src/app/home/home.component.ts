import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  name='Mariem'

  constructor(private http: HttpClient) {
    this.http.get('').subscribe(data=>{console.log(data)})
  }

  ngOnInit(): void {

  }

}
