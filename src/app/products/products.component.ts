
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProductsService } from '../services/products.service';


@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent   {

    products : any[] = [];

    constructor(private route :Router, private  productsService:ProductsService) {

    }

    ngOnInit(): void {
      this.getproducts();
    }

    getproducts(){
      this.productsService.getProducts().subscribe( data => {
        this.products = data;
      });
    }
}
